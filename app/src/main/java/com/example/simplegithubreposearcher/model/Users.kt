package com.example.simplegithubreposearcher.model

data class Users(
    val total_count : Int,
    val incomplete_results:Boolean,
    val items : List<User>
)
