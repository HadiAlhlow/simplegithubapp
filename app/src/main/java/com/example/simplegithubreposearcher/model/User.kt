package com.example.simplegithubreposearcher.model

data class User(
    val avatar_url: String,
    val html_url: String,
    val id: Int,
    val login: String,
    val repos_url: String,
    val type: String,
    val url: String,
    val name:String,
    val followers:Int,
    val following:Int,
    val bio:String

)