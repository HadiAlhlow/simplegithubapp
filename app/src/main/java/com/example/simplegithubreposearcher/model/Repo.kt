package com.example.simplegithubreposearcher.model

data class Repo(
    val id: Int,
    val node_id: String,
    val name: String,
    val full_name: String,
    val private: Boolean,
    val language: String
)
