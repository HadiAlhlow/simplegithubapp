package com.example.simplegithubreposearcher.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.simplegithubreposearcher.adapter.SearchAdapter
import com.example.simplegithubreposearcher.databinding.FragmentSearchBinding
import com.example.simplegithubreposearcher.viewModels.SearchViewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : Fragment() {

    @Inject
    lateinit var searchAdapter: SearchAdapter
    private lateinit var searchViewModel: SearchViewModel
    private val TAG = "Fragment Search"
    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Toast.makeText(context, "Please Click on search Icon to search for user", Toast.LENGTH_LONG)
            .show()


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        searchViewModel = ViewModelProvider(this)[SearchViewModel::class.java]



        searchViewModel.usersList.observe(viewLifecycleOwner) {
            searchAdapter.swapData(it)

            Log.d(TAG, "onViewCreated: $it")
        }

        setUpRecyclerView()



        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(q: String?): Boolean {

                if (q != null) {
                    searchViewModel.getSearchedUsers(q)
                    binding.searchRv.isVisible = true

                    Log.d(TAG, q.toString())
                } else {
                    Log.d(TAG, q.toString())
                    Log.d(TAG, "onQueryTextSubmit: nothing in ")
                }


                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {


                return false
            }

        })


        searchAdapter.click {
            Navigation.findNavController(binding.root).navigate(
                SearchFragmentDirections.actionSearchFragmentToDetailFragment(Gson().toJson(it))
            )
        }


    }

    private fun setUpRecyclerView() = binding.searchRv.apply {
        adapter = searchAdapter
        layoutManager = LinearLayoutManager(context)

    }
}