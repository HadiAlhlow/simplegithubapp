package com.example.simplegithubreposearcher.ui

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.simplegithubreposearcher.databinding.FragmentDetailBinding
import com.example.simplegithubreposearcher.model.User
import com.example.simplegithubreposearcher.viewModels.DetailViewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {
    private val args: DetailFragmentArgs by navArgs()
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private lateinit var detailViewModel: DetailViewModel
    private var userLogin = ""
    private var repoLogin = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        detailViewModel = ViewModelProvider(this)[DetailViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val jsonString = args.user
        val user = Gson().fromJson(jsonString, User::class.java)

        userLogin = user.login

        detailViewModel.getDetails(user.login)

        detailViewModel.details.observe(viewLifecycleOwner) {
            //Details Fragment User Bio
            val spBio = SpannableStringBuilder().bold { append("Bio:") }.append(" " + it.bio)
            binding.UserDetailsBio.text = spBio

            //Details Fragment User FollowersCount
            val spFollowers = SpannableStringBuilder().bold { append("Followers:\n") }
                .append("     " + it.followers.toString())
            binding.UserDetailsFollowers.text = spFollowers

            //Details Fragment User FollowingCount
            val spFollowing = SpannableStringBuilder().bold { append("Following:\n") }
                .append("     " + it.following.toString())
            binding.UserDetailsFollowing.text = spFollowing
            //Details Fragment User Name
            val spName = SpannableStringBuilder().bold { append("Name:") }.append(" " + it.name)
                .append("\n" + "Login: " + it.login)
            binding.UserDetailsName.text = spName

            repoLogin = it.login

            Glide.with(requireContext()).load(it.avatar_url).transition(
                DrawableTransitionOptions.withCrossFade()
            ).centerCrop().into(binding.UserDetailsImage)

        }


        binding.UserDetailsRepoBtn.setOnClickListener {
            Navigation.findNavController(binding.root)
                .navigate(DetailFragmentDirections.actionDetailFragmentToReposFragment(repoLogin))

        }

    }

}