package com.example.simplegithubreposearcher.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.simplegithubreposearcher.adapter.RepoRvAdapter
import com.example.simplegithubreposearcher.databinding.FragmentReposBinding
import com.example.simplegithubreposearcher.viewModels.ReposViewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ReposFragment : Fragment() {

    @Inject
    lateinit var reposAdapter: RepoRvAdapter
    private lateinit var repoViewModel: ReposViewModel
    private val TAG = "Fragment Repo"
    private val args: ReposFragmentArgs by navArgs()
    private var _binding: FragmentReposBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentReposBinding.inflate(inflater, container, false)
        repoViewModel = ViewModelProvider(this)[ReposViewModel::class.java]
        val jsonString = args.repoUrl
        val user = Gson().fromJson(jsonString, String::class.java)

        Log.d(TAG, "onCreateView: $user")

        repoViewModel.getSearchedRepos(user)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        repoViewModel.repoList.observe(viewLifecycleOwner) {

            reposAdapter.swapData(it)
        }


        setUpRecyclerView()

        reposAdapter.click {
            val repoUrl = "https://github.com/" + it.full_name
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(repoUrl)
            startActivity(i)
        }
    }

    private fun setUpRecyclerView() = binding.rvRepos.apply {
        adapter = reposAdapter
        layoutManager = LinearLayoutManager(context)
    }

}