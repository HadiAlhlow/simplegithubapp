package com.example.simplegithubreposearcher.viewModels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.simplegithubreposearcher.model.Repo
import com.example.simplegithubreposearcher.network.GithubApi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class ReposViewModel @Inject constructor(private val githubApi: GithubApi) : ViewModel() {
    private var _repoList = MutableLiveData<List<Repo>>()
    val repoList: LiveData<List<Repo>> get() = _repoList
    private val TAG = "ReposViewModel"


    fun getSearchedRepos(q: String) {
        viewModelScope.launch {
            val response = try {

                githubApi.getSearchedRepos(q)

            } catch (e: IOException) {
                Log.e(TAG, e.toString())
                null
            } catch (e: HttpException) {
                Log.e(TAG, e.toString())
                null
            } catch (e: Exception) {
                Log.e(TAG, e.toString())
                null
            }
            if (response?.body() != null && response.isSuccessful) {
                withContext(Dispatchers.Main) {
                   _repoList.value = response.body()!!
                    Log.d(TAG, response.body()!!.toString())
                    Log.d(TAG, response.code().toString())
                }
            } else {

                Log.e(TAG, response?.errorBody().toString())
                Log.e(TAG, response?.code().toString())
            }
        }
    }
}