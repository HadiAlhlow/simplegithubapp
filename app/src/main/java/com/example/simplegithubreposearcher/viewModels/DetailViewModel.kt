package com.example.simplegithubreposearcher.viewModels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.simplegithubreposearcher.model.User
import com.example.simplegithubreposearcher.network.GithubApi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject


@HiltViewModel
class DetailViewModel @Inject constructor(private val gitApiService: GithubApi) : ViewModel() {
    private val TAG = "DetailViewModel"
    private var _details = MutableLiveData<User>()
    val details: LiveData<User> get() = _details

    fun getDetails(userName: String) {
        viewModelScope.launch {
            val response = try {

                gitApiService.getUserDetails(userName)


            } catch (e: IOException) {
                Log.e(TAG, "IOException, you might not have internet connection")
                null
            } catch (e: HttpException) {
                Log.e(TAG, "HttpException, unexpected response")
                null
            }
            if (response?.body() != null && response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    _details.value = response.body()
                }
            } else {
                Log.e(TAG, "Response not successful")
            }
        }
    }
}