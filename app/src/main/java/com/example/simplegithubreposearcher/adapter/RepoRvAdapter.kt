package com.example.simplegithubreposearcher.adapter

import android.annotation.SuppressLint
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.recyclerview.widget.RecyclerView
import com.example.simplegithubreposearcher.databinding.ReposCvBinding
import com.example.simplegithubreposearcher.model.Repo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepoRvAdapter @Inject constructor() : RecyclerView.Adapter<RepoVh>() {
    private var currentItem: Repo? = null
    private var repoList: List<Repo> = listOf()
    private lateinit var click: (repo: Repo) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoVh {
        val binding = ReposCvBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return RepoVh(binding)
    }

    override fun onBindViewHolder(holder: RepoVh, position: Int) {
        currentItem = repoList[position]
        holder.binding.apply {
            val repName =
                SpannableStringBuilder().bold { append("Repo Name: " + " " + currentItem?.name) }
            val repLang =
                SpannableStringBuilder().bold { append("Develop Lang: " + " " + currentItem?.language) }
            val repPrivecy =
                SpannableStringBuilder().bold { append("Private: " + " " + currentItem?.private.toString()) }

            repoLang.text = repLang
            repoName.text = repName
            repoPrivecy.text = repPrivecy

        }
        holder.binding.root.setOnClickListener {
            click(repoList[position])

        }

    }

    override fun getItemCount(): Int {
        return repoList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun swapData(data: List<Repo>) {
        this.repoList = data
        notifyDataSetChanged()
    }

    fun click(callback: (repo: Repo) -> Unit) {
        this.click = callback
    }
}

class RepoVh(val binding: ReposCvBinding) : RecyclerView.ViewHolder(binding.root)