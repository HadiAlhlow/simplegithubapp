package com.example.simplegithubreposearcher.adapter

import android.annotation.SuppressLint
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.simplegithubreposearcher.databinding.CvUserBinding
import com.example.simplegithubreposearcher.model.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchAdapter @Inject constructor() : RecyclerView.Adapter<SearchVh>() {

    private var currentItem: User? = null
    private var dataList: List<User> = listOf()
    private lateinit var click: (user: User) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchVh {
        val binding = CvUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)


        return SearchVh(binding)
    }

    override fun onBindViewHolder(holder: SearchVh, position: Int) {

        currentItem = dataList[position]

        holder.binding.apply {
            val userName =
                SpannableStringBuilder().append("Userame:")
                    .bold { append(" " + currentItem!!.login) }

            UserName.text = userName

            Glide.with(holder.binding.root).load(currentItem?.avatar_url).transition(
                DrawableTransitionOptions.withCrossFade()
            ).centerCrop().into(UserImage)


            holder.binding.root.setOnClickListener {
                click(dataList[position])

            }
        }

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun swapData(data: List<User>) {
        this.dataList = data
        notifyDataSetChanged()
    }

    fun click(callback: (user: User) -> Unit) {
        this.click = callback
    }
}

class SearchVh(val binding: CvUserBinding) : RecyclerView.ViewHolder(binding.root)