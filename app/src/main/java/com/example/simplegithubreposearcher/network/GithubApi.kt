package com.example.simplegithubreposearcher.network

import com.example.simplegithubreposearcher.model.Repo
import com.example.simplegithubreposearcher.model.User
import com.example.simplegithubreposearcher.model.Users
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubApi {

    @GET("users/{user}")
    @Headers("Authorization: token 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")
    suspend fun getUserDetails(@Path("user") username: String): Response<User>

    @GET("search/users")
    @Headers("Authorization: token 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")
    suspend fun getSearchedUsers(@Query("q") q: String): Response<Users>

    @GET("users/{user}/repos")
    @Headers("Authorization: token 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")
    suspend fun getSearchedRepos(@Path("user") username: String): Response<List<Repo>>
}
